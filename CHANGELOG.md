## Distant Fixes: Rise of House Telvanni Changelog

#### 1.3

* Removed plugin declaration of script that was removed (thank you [David Hanina](https://gitlab.com/duzda)!)

[Download Link](https://gitlab.com/modding-openmw/distant-fixes-rise-of-house-telvanni/-/packages/34041639) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53138)

#### 1.2

* Optimized scripts

[Download Link](https://gitlab.com/modding-openmw/distant-fixes-rise-of-house-telvanni/-/packages/30755247) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53138)

#### 1.1

* Fixed incorrect usage of `StartScript` which would cause major FPS issues.

[Download Link](https://gitlab.com/modding-openmw/distant-fixes-rise-of-house-telvanni/-/packages/30744227) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53138)

#### 1.0

* Initial release of the mod.

[Download Link](https://gitlab.com/modding-openmw/distant-fixes-rise-of-house-telvanni/-/packages/15558984) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53138)
