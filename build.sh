#!/bin/sh
set -e

name=DistantFixesRiseOfHouseTelvanni
delta_plugin convert $name.yaml
mv $name.omwaddon $name.esp

delta_plugin convert ${name}UL.yaml
mv ${name}UL.omwaddon ${name}UL.esp
