# Distant Fixes: Rise of House Telvanni

Prevents distant things from popping out and in at various points in the story. **Only for OpenMW 0.47.0 or newer!**

## About

OpenMW's object paging system is great but it also exposes a detail of how the game is implented: that being a cell's scripts won't run until the cell is loaded. That means if a structure gets added as part of the story but at first does not exist, you'll get an awkward pop out as you approach the cell and it actually disables as intended when the cell loads. The reverse can happen as well; when the structure is built, it will pop in as you approach just as it popped out before.

The [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236) mod by Hemaris fixes this for the Great House content as well as Ghostfence in the base Morrowind game, but there are several mods that add structures and suffer from this effect.

This mod fixes the issue for [Rise of House Telvanni](https://www.nexusmods.com/morrowind/mods/27545) and is compatible with [the "2.0" patch for it](https://www.nexusmods.com/morrowind/mods/48225).

Also included is an optional patch for the [Uvirith's Legacy](https://stuporstar.sarahdimento.com/) RoHT add-on plugin.

#### Credits

**Author**: johnnyhostile

**Special Thanks**:

* Benjamin Winger for making [Delta Plugin](https://gitlab.com/bmwinger/delta-plugin/)
* Hemaris for making [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236) and inspring me to make this
* mort, Pozzo, Karpik777, and bhl for their work on [Rise of House Telvanni](https://www.nexusmods.com/morrowind/mods/27545) ([2.0](https://www.nexusmods.com/morrowind/mods/48225))
* Stuporstar for making [Uvirith's Legacy](https://stuporstar.sarahdimento.com/)
* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Project Home](https://modding-openmw.gitlab.io/distant-fixes-rise-of-house-telvanni/)

<!--[Nexus Mods](https://www.nexusmods.com/morrowind/mods/TODO)-->

[Source on GitLab](https://gitlab.com/modding-openmw/distant-fixes-rise-of-house-telvanni)

#### Installation

**This mod requires OpenMW 0.47.0 or newer!**

[This short video](https://www.youtube.com/watch?v=xzq_ksVuRgc) demonstrates the whole process in under two minutes.

1. Download the zip from a link above
1. Extract the contents to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\DistantFixes\distant-fixes-rise-of-house-telvanni

        # Linux
        /home/username/games/OpenMWMods/DistantFixes/distant-fixes-rise-of-house-telvanni

        # macOS
        /Users/username/games/OpenMWMods/DistantFixes/distant-fixes-rise-of-house-telvanni
1. Add the appropriate data path to your `openmw.cfg` file, for example:

        data="C:\games\OpenMWMods\DistantFixes\distant-fixes-rise-of-house-telvanni"
1. Enable the mod plugin via OpenMW-Launcher, or add this to `openmw.cfg` ([official OpenMW documentation](https://openmw.readthedocs.io/en/latest/reference/modding/mod-install.html#install)):

        content=DistantFixesRiseOfHouseTelvanni.esp
        content=DistantFixesRiseOfHouseTelvanniUL.esp
1. You should load this plugin right after `ROHT_2_0_8.ESP` if you're using that, otherwise it can go anywhere in your load order.
1. The optional UL add-on plugin should be loaded after `UL_3.5_RoHT_1.52_Add-on.esp`.

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/distant-fixes-rise-of-house-telvanni/-/issues/new) for bug reports or feature requests
* Email: `johnnyhostile at modding-openmw dot com`
<!--* Leave a comment [on Nexus mods](https://www.nexusmods.com/morrowind/mods/TODO?tab=posts)-->

#### Testing (SPOILER WARNING)

<details>
<summary>How to see this work without playing all of RoHT (click to expand, SPOILER WARNING):</summary>

**For the changes near Kogoruhn, run the following in the console with the area in view:**

```
startscript momw_df_roht_journact1s
set RoHT_TelKR_status to 1
```

**For the changes near Statue of Azura, run the following in the console with the area in view:**

```
startscript momw_df_roht_journact2s
set RoHT_TelAzura_status to 1
```

**For the changes near Nchuleft Ruin, run the following in the console with the area in view:**

```
startscript momw_df_roht_journact3s
startscript momw_df_roht_ul_journacts ; only do this if you're using the optional UL addon patch
set RoHT_TelDwemeris_status to 1 ; initial stage
set RoHT_TelDwemeris_status to 2 ; fully built
```

**For the changes near Ebonheart, run the following in the console with the area in view:**

```
startscript momw_df_roht_journact4s
set RoHT_EHCouncil_status to 1 ; initial stage
set RoHT_EHCouncil_status to 2 ; fully built
```

**For the changes near Dagon Fel, run the following in the console with the area in view:**

```
startscript momw_df_roht_journact5s
set RoHT_TelLlarelah_status to 2 ; initial stage
set RoHT_TelLlarelah_status to 3 ; fully built
```

**For the changes at Telasero, run the following in the console with the area in view:**

```
startscript momw_df_roht_journact6s
set RoHT_Telasero_status to 1 ; added Stoh-Gei's Dome home
set RoHT_Telasero_status to 2 ; fully expanded Telasero
```
</details>

<details>
<summary>TODO, stuff I may have missed (click to expand, SPOILER WARNING):</summary>

When to enable these?

```
# Tel Dwemeris
RoHT_Portal_Vvardenfel_1024_116
RoHT_TelDwem_slavemarket
RoHT_TelDwem_Walker00
RoHT_TelDwem_Walker10
RoHT_TelDwem_Walker20
```

</details>

